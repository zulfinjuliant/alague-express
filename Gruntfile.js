module.exports = function(grunt) {
  grunt.initConfig({
    less: {
      theme: {
        options: {
          compress: true,
          yuicompress: true,
          optimization: 2
        },
        files: {
          "public/css/theme.css": "public/less/theme.less"
        }
      }
    },
    watch: {
      styles: {
        files: ['public/**/*.less'], // which files to watch
        tasks: ['less'],
        options: {
          nospawn: true
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', ['watch']);
};