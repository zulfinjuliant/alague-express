/**
 * HERE IS APP CONFIGURATION FOR DEV ENVIRONMENT
 * -----------------------------------------------------
 * if you are in dev environment(look at ./server.js), 
 * this configuration will append general configuration 
 * in ./app/configs/app.js file
 * ------------------------------------------------------
 * in this example, we use port 3003 for dev mode 
 * instead port 3000 (in general app.js configuration)
 */

module.exports = {

	port: 3003

}